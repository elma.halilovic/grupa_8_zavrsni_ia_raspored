package START.PRODEKAN.grupa;

import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Node;

import java.awt.*;
import java.io.IOException;

public class grupaController {

    public void nazad(ActionEvent actionEvent) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource("../sampleProdekan.fxml"));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle("Prodekan");
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



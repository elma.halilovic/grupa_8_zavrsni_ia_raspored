package START;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StartMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sampleStart.fxml"));
        primaryStage.setTitle("Dobro došli");
        primaryStage.setScene(new Scene(root, 680, 500));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

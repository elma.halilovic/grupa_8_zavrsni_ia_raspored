package START.PROFESOR;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class profesorController {

    public static List<Stage> stagev = new ArrayList<Stage>();
    public static List<String> fxmlv = new ArrayList<String>();

    public static Stage stage;

    public void akcija(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText();;
        stagev.add((Stage) ((Node) actionEvent.getSource()).getScene().getWindow());
        fxmlv.add("../PROFESOR/sampleProfesor.fxml");

        System.out.println(text);
        if (text.equals("Dodaj rezervaciju")) {
            uradi(actionEvent, "Dodajte rezervaciju", "../PROFESOR/dodaj_rezervaciju/dodaj_rezervaciju.fxml");
        }

        else if (text.equals("Izbriši rezervaciju")) {
            uradi(actionEvent, "Ibrišite rezervaciju", "../PROFESOR/obrisi_rezervaciju/obrisi_rezervaciju.fxml");
        }

        else if (text.equals("Izmijeni rezervaciju")) {
            uradi(actionEvent, "Izmijenite rezervaciju", "../PROFESOR/izmijeni_rezervaciju/izmijeni_rezervaciju.fxml");
        }
        else if (text.equals("Mjesečni izvještaj")) {
            uradi(actionEvent, "Mjesečni izvještaj", "../PROFESOR/mjesecni_izvjestaj/mjesecni_izvjestaj.fxml");
        }

        else if (text.equals("Odjavite se")) {
            uradi(actionEvent, "Dobro došli", "../sampleStart.fxml");
        }
    }

    public void uradi(ActionEvent actionEvent, String title, String path) {
        try {
            Parent view2 = FXMLLoader.load(getClass().getResource(path));
            Scene scene2 = new Scene(view2);
            Stage newWindow = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
            newWindow.setTitle(title);
            newWindow.centerOnScreen();
            newWindow.setScene(scene2);
            newWindow.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
